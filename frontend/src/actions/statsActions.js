import { FETCH_AUTHOR_STATS, FETCH_PICKED_AUTHORS_STATS } from './types';

// Fetches all stats from localhost:8080/stats/ if url_name == false
export const fetchAuthorStats = (url_name, ACTION_TO_FIRE) => dispatch => {
    dispatch({
      type: FETCH_AUTHOR_STATS,
      url_name,
      ACTION_TO_FIRE
    });
}

export const subtractStats = () => dispatch => {
  dispatch({
    type: FETCH_PICKED_AUTHORS_STATS
  });
}
