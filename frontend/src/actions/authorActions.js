import { FETCH_AUTHORS, POPULATE_AUTHORS, PICK_AUTHOR, UNPICK_AUTHOR, PICK_ALL_AUTHORS } from './types';

export const fetchAuthors = () => dispatch => {
    dispatch({
      type: FETCH_AUTHORS
    });
}

export const populateAuthors = (payload) => dispatch => {
    dispatch({
      type: POPULATE_AUTHORS,
      payload
    });
}

export const pickAuthor = (url_name, name) => dispatch => {
  var payload = {
    url_name,
    name
  }

  dispatch({
    type: PICK_AUTHOR,
    payload
  });
}

export const pickAllAuthors = () => dispatch => {
  dispatch({
    type: PICK_ALL_AUTHORS
  });
}

export const unpickAuthor = (url_name, name) => dispatch => {
  var payload = {
    url_name,
    name
  }

  dispatch({
    type: UNPICK_AUTHOR,
    payload
  });
}
