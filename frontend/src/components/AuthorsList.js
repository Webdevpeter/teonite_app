import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchAuthors, pickAuthor, pickAllAuthors } from '../actions/authorActions';
import { fetchAuthorStats } from '../actions/statsActions';
import { POPULATE_STATS, FETCH_ALL_STATS } from '../actions/types';

class AuthorsList extends React.Component {

  componentWillMount() {
    this.props.fetchAuthors();
  }

  renderAuthors() {
    let authorsJSX = [];
    for (let i= 0; i < this.props.allAuthors.length; i++) {
      let author = this.props.allAuthors[i];
      authorsJSX.push(<li key={'AuthorsList_'+author.url_name} onClick={() => this.clickHandler(author.url_name, author.name)} className="list__item--author">{author.name}</li>)
    }
    return authorsJSX;
  }

  clickHandler(url_name, name) {
    this.props.pickAuthor(url_name, name);
    this.props.fetchAuthorStats(url_name, POPULATE_STATS);
  }

  clickAllHandler() {
    this.props.pickAllAuthors();
    this.props.fetchAuthorStats(false, FETCH_ALL_STATS);
  }

  render() {
    return (
      <div className="authors-list">
        <h1>Authors</h1>
        <ul className="list--regular">
          {this.renderAuthors()}
          <li key={'AuthorsList_allauthors'} onClick={() => this.clickAllHandler()} className="list__item--author">All authors</li>
        </ul>
      </div>
    )
  }
}

AuthorsList.propTypes = {
  pickAuthor: PropTypes.func.isRequired,
  fetchAuthors: PropTypes.func.isRequired,
  pickAllAuthors: PropTypes.func.isRequired,
  fetchAuthorStats: PropTypes.func.isRequired,
  allAuthors: PropTypes.array.isRequired
}

const mapStateToProps = state => ({
  allAuthors: state.authors.all_authors
});

export default connect(mapStateToProps, { fetchAuthors, pickAuthor, pickAllAuthors, fetchAuthorStats })(AuthorsList);
