import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class Table extends React.Component {

  renderRows() {
    let data = this.props.stats;
    let authorsJSX = [];

    for(let i = 0; i < data.length; i++) {
      authorsJSX.push(<tr key={'Table_'+data[i].word}><td>{i+1}</td><td>{data[i].word}</td><td>{data[i].count}</td></tr>);
    }

    return authorsJSX;
  }

  render() {
    return (
      <div className="results">
        <table>
          <thead>
            <tr><td></td><td>Word</td><td>Count</td></tr>
          </thead>
          <tbody>
            {this.renderRows()}
          </tbody>
        </table>
      </div>
    )
  }
}

Table.propTypes = {
  stats: PropTypes.array.isRequired
}

const mapStateToProps = state => ({
  stats: state.stats.stats_to_print
});

export default connect( mapStateToProps, null)(Table);
