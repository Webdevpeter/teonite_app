import React from 'react';
import AuthorsList from './AuthorsList';
import AuthorsPicker from './AuthorsPicker';
import Table from './Table';

class App extends React.Component {

  render() {
    return (
      <div className="application">
        <AuthorsPicker />
        <AuthorsList />
        <Table />
      </div>
    )
  }
}

export default App;
