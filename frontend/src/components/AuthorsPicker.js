import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { unpickAuthor } from '../actions/authorActions';
import { subtractStats } from '../actions/statsActions';

class AuthorsPicker extends React.Component {

  renderAuthors() {
    let authors = [];

    for (let i = 0; i < this.props.pickedAuthors.length; i++) {
      let author = this.props.pickedAuthors[i];
      authors.push(<li key={'AuthorsPicker_'+author.url_name} className="author--picked">{author.name}<span onClick={() => this.clickHandler(author.url_name, author.name)} className="author__close">x</span></li>);
    }

    return authors;
  }

  clickHandler(url_name, name) {
    this.props.unpickAuthor(url_name, name);
    this.props.subtractStats();
  }

  render() {
    return (
      <ul className="authors-picker">
        {this.renderAuthors()}
      </ul>
    )
  }
}

AuthorsPicker.propTypes = {
  unpickAuthor: PropTypes.func.isRequired,
  subtractStats: PropTypes.func.isRequired,
  pickedAuthors: PropTypes.array.isRequired
}

const mapStateToProps = state => ({
  pickedAuthors: state.authors.picked_authors
});

export default connect(mapStateToProps, {unpickAuthor, subtractStats})(AuthorsPicker);
