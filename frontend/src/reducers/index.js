import { combineReducers } from 'redux';
import authorsReducer from './authorsReducer';
import statsReducer from './statsReducer';

export default combineReducers({
  authors: authorsReducer,
  stats: statsReducer
})
