import { POPULATE_STATS, SUBTRACT_STATS, FETCH_ALL_STATS } from '../actions/types';

const initialState = {
  stats: [],
  stats_to_print: [],
  cache: {}
}

export default function(state = initialState, action) {
  let currStats, currCache, newStats, name, sorted, spliced;
  switch(action.type) {
    case POPULATE_STATS: 
      currStats = state.stats.slice();
      newStats = action.payload.stats;
      currCache = Object.assign({}, state.cache);
      name = action.payload.url_name;

      if(!currCache[name]) {
        currCache[name] = newStats.slice();
      }

      newStats.forEach((newStat) => {
        let statIndex = contains(currStats, newStat);
        if(statIndex) {
          currStats[statIndex.index].count += newStat.count;
        } else {
          currStats.push(Object.assign({}, newStat));
        }
      });

      sorted = currStats.sort((a, b) => b.count - a.count);
      spliced = sorted.slice(0, 10);

      return {
        ...state,
        stats: sorted,
        stats_to_print: spliced,
        cache: currCache
      }
    case SUBTRACT_STATS:
      newStats = action.payload;
      currCache = Object.assign({}, state.cache);
      name = action.payload.url_name;

      if(!currCache[name]) {
        currCache[name] = newStats.slice();
      }

      sorted = newStats.sort((a, b) => b.count - a.count);
      spliced = sorted.slice(0, 10);

      return {
        ...state,
        stats: sorted,
        stats_to_print: spliced,
        cache: currCache
      }
    case FETCH_ALL_STATS:
      return {
        ...state,
        stats_to_print: action.payload.stats.slice()
      }
    default:
      return state;
  }
}

// Returns { index: i } if contains; else false
function contains(oldStats, newStat) {
  let answer = false;
  oldStats.forEach((oldStat, index) => {
    if(oldStat.word === newStat.word) answer = { index };
  });
  return answer;
}
