import { POPULATE_AUTHORS, PICK_AUTHOR, UNPICK_AUTHOR, PICK_ALL_AUTHORS } from '../actions/types';

const initialState = {
  all_authors: [],
  picked_authors: []
}

export default function(state = initialState, action) {
  switch(action.type) {
    case POPULATE_AUTHORS:
      let all_authors = action.payload;

      all_authors = sortAbc(all_authors);

      return {
        ...state,
        all_authors
      }
    case PICK_AUTHOR:
      // Get picked & all authors from state
      let picked_authors = state.picked_authors.slice();
      all_authors = state.all_authors.slice();

      // Remove picked author from AuthorsList
      if(containsAuthor(all_authors, action.payload)) {
        all_authors.splice(containsAuthor(all_authors, action.payload).index, 1);
      }

      // Add picked author to PickedList
      if (!containsAuthor(picked_authors, action.payload)) {
        picked_authors.push(action.payload);
      }

      all_authors = sortAbc(all_authors);
      picked_authors = sortAbc(picked_authors);

      return {
        ...state,
        all_authors,
        picked_authors
      }
    case PICK_ALL_AUTHORS:
      // Get picked & all authors from state
      picked_authors = state.picked_authors.slice();
      all_authors = state.all_authors.slice();

      // Move all authors to picked authors array
      picked_authors = picked_authors.concat(all_authors)
      all_authors.splice(0, all_authors.length);

      picked_authors = sortAbc(picked_authors);

      return {
        ...state,
        all_authors,
        picked_authors
      }
    case UNPICK_AUTHOR:
      // Get picked & all authors from state
      picked_authors = state.picked_authors.slice();
      all_authors = state.all_authors.slice();

      // Move unpicked author from picked authors to all authors
      picked_authors.splice(containsAuthor(picked_authors, action.payload).index, 1);
      all_authors.push(action.payload);

      all_authors = sortAbc(all_authors);

      return {
        ...state,
        all_authors,
        picked_authors
      }
    default:
      return state;
  }
}

// Returns {index: i} if contains; else false
function containsAuthor(arr, author) {
  for(let i = 0; i < arr.length; i++) {
    if(arr[i].url_name === author.url_name) {
      return {index: i};
    }
  }
  return false;
}

// Returns an array sorted alphabetically. Doesn't modify the original one.
function sortAbc(arr = []) {
  let temp = arr.slice();
  temp.sort(function(a, b){
      if(a.url_name < b.url_name) return -1;
      if(a.url_name > b.url_name) return 1;
      return 0;
  });
  return temp;
}
