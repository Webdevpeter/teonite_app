import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import App from './components/App';
import './index.css';
import { Provider } from 'react-redux';

import store from './store';

ReactDOM.render(
  (<BrowserRouter>
    <Provider store={store}>
      <Switch>
        <Route path='/' exact component={App} />
        <Route path='/stats' exact component={App} />
      </Switch>
    </Provider>
  </BrowserRouter>),
  document.getElementById('root')
);
