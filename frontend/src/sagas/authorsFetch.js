import {call, put, takeEvery } from 'redux-saga/effects';
import { FETCH_AUTHORS, POPULATE_AUTHORS } from '../actions/types';

export function* fetchAuthors() {
  try {
    let payload = [];
    let data = yield call(get);
    for (let url_name in data) {
      payload.push({name: data[url_name], url_name});
    }
    yield put({type: POPULATE_AUTHORS, payload });
  } catch (error) {
    console.error(error);
  }
}

export function* watchAuthors() {
  yield takeEvery(FETCH_AUTHORS, fetchAuthors);
}

function get() {
  return fetch('http://localhost:8080/authors/')
    .then(res => res.json())
    .then(data => data);
}
