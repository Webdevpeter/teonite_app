import {call, put, select, takeEvery } from 'redux-saga/effects';
import { SUBTRACT_STATS, FETCH_AUTHOR_STATS, FETCH_PICKED_AUTHORS_STATS } from '../actions/types';

export const getStats = state => Object.assign(state.stats);
export const getPicked = state => state.authors.picked_authors;

export function* fetchAuthorStats(action) {
  try {

      let stats = yield select(getStats);

      let payload = {
        stats: [],
        url_name: action.url_name
      };

      if(!action.url_name || !stats.cache[action.url_name]) {
        // If there's no author in cache, get if from API
        let urlModifier = action.url_name ? action.url_name+'/' : ''
        let url = 'http://localhost:8080/stats/'+urlModifier;
        let data = yield call(get, url);

        for (let word in data) {
          payload.stats.push({word, count: data[word]});
        }
      } else {
        payload.stats = stats.cache[payload.url_name].slice();
      }

      yield put({type: action.ACTION_TO_FIRE, payload });

  } catch (error) {
    console.error(error);
  }
}

export function* fetchPickedAuthorsStats(action) {
  try {
      let picked_authors = yield select(getPicked);
      let stats = yield select(getStats);

      let allWords = {};
      let payload = [];

      // Get all currently picked (after unpicking) author words and their counts
      for(let i = 0; i < picked_authors.length; i++) {
        if(!stats.cache[picked_authors[i].url_name]) {
          // From backend API
          let url = 'http://localhost:8080/stats/'+picked_authors[i].url_name+'/';
          let data = yield call(get, url);
          for (let word in data) {
            if(!allWords[word]) {
              allWords[word] = data[word];
            } else {
              allWords[word] += data[word];
            }
          }
        } else {
          // From state's cache
          stats.cache[picked_authors[i].url_name].forEach((el) => {
            if(!allWords[el.word]) {
              allWords[el.word] = el.count;
            } else {
              allWords[el.word] += el.count;
            }
          });
        }
      }
      // Craft structure desired by stats reducer
      for (let word in allWords) {
        payload.push({word, count: allWords[word]});
      }

      yield put({type: SUBTRACT_STATS, payload });

  } catch (error) {
    console.error(error);
  }
}

export function* watchStats() {
  yield takeEvery(FETCH_AUTHOR_STATS, fetchAuthorStats);
  yield takeEvery(FETCH_PICKED_AUTHORS_STATS, fetchPickedAuthorsStats);
}

function get(url) {
  return fetch(url)
    .then(res => res.json())
    .then(data => data);
}
