import {all} from 'redux-saga/effects';
import { watchAuthors } from './authorsFetch';
import { watchStats } from './statsFetch';

export default function* rootSaga() {
  yield all([
    watchAuthors(),
    watchStats(),
  ]);
}
