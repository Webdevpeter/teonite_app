# Teonite Crawler Backend

## Build instructions
1. `git clone https://gitlab.com/Webdevpeter/teonite_app.git`
2. `cd teonite_app`
3. `docker-compose up`
4. Wait for crawler to finish to see results on endpoints.
