from rest_framework import serializers
from stats.models import Authors

class AuthorsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Authors
        fields = ['name', 'url_name']

class WordsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Authors
        fields = ['url_name', 'all_words']
