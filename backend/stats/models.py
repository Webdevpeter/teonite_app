from django.db import models

class Authors(models.Model):
    name = models.CharField(max_length=200)
    all_words = models.TextField()
    url_name = models.CharField(max_length=200, unique=True)
    def __str__(self):
        return self.url_name
    class Meta:
        verbose_name_plural = "Authors"
