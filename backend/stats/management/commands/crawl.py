import requests
from bs4 import BeautifulSoup
from stats.models import Authors
import re
from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
    help = 'Crawls Teonite Blog and saves data to db.'

    def handle(self, *args, **options):
        crawled_data = crawlTeonite()
        crawlerUpdateAuthors(crawled_data)

        return

def crawlTeonite():
    domain = 'https://teonite.com'
    crawled_data = {
        'posts': [],
        'authors': []
    }
    i = 1
    while True:
        # Get blog list page starting from 1st
        r = requests.get(domain + '/blog/page/' + str(i))
        soup = BeautifulSoup(r.content, 'lxml')
        # Get all post links from the page
        post_links_on_page = soup.find_all('a', {'class': 'read-more'})
        # Stop the loop if there are no posts on current page
        if(len(post_links_on_page) < 1):
            break
        # Loop through the current page's post links
        for post_link in post_links_on_page:
            # Get post
            post_r = requests.get(domain + post_link.get("href"))
            post_soup = BeautifulSoup(post_r.content, 'lxml')
            # Regex to cut out all non-letter characters excluding spaces
            letters_spaces_regex = re.compile('[^a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ ]')
            # Get data from post soup
            post = {
                'post_url': domain + post_link.get("href"),
                'post_title': letters_spaces_regex.sub('', post_soup.h1.text).strip(),
                'post_content': re.sub(' +',' ', letters_spaces_regex.sub('', post_soup.find('section', {'class': 'post-content'}).text)).strip(),
                'post_author': post_soup.find('span', {'class': 'author-content'}).h4.text.strip()
            }
            print('Page: ' + str(i))
            print('Post: ' + str(len(crawled_data['posts']) + 1))
            print(post['post_content'])
            crawled_data['posts'].append(post)
            if not (post['post_author'] in crawled_data['authors']):
                crawled_data['authors'].append(post['post_author'])
        i += 1
    return crawled_data

def crawlerUpdateAuthors(crawled_data):
    Authors.objects.all().delete()
    for author in crawled_data['authors']:
        author_url_name = author.replace(" ", "").lower().replace("ą", "a").replace("ć", "c").replace("ę", "e").replace("ł", "l").replace("ń", "n").replace("ó", "o").replace("ś", "s").replace("ź", "z").replace("ż", "z")
        authors_words = []
        for post in crawled_data['posts']:
            if author == post['post_author']:
                authors_words.append(post['post_content'] + ' ')
        author_to_save = Authors.objects.create(name = author, all_words = ''.join(authors_words), url_name = author_url_name)
