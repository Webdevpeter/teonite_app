def countWords(words_arr):
    temp_data = {}
    # Count words
    for word in words_arr:
        if not temp_data.get(word):
            temp_data[word] = 1
        else:
            temp_data[word] += 1
    # Sort words
    sorted_words = sorted(temp_data, key=temp_data.get, reverse=True)
    # Get 10 highest count words
    answer = {}
    i = 0
    for word in sorted_words:
        if i == 10:
            break
        answer[word] = temp_data[word]
        i += 1
    return answer
